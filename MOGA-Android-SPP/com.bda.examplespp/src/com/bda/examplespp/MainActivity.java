/////////////////////////////////////////////////////////////////////////////////
//
//	MainActivity.java
//	Unity Android SPP Tutorial Sample Project
//	� 2014 Bensussen Deutsch and Associates, Inc. All rights reserved.
//
//	description:	Shows how to use a Controller on Android in SPP mode.
//					Completed Tutorial Project.
//
/////////////////////////////////////////////////////////////////////////////////

package com.bda.examplespp;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import com.bda.controller.Controller;
import com.bda.controller.ControllerListener;
import com.bda.controller.KeyEvent;
import com.bda.controller.MotionEvent;
import com.bda.controller.StateEvent;

import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.app.Activity;
import android.widget.FrameLayout;

public class MainActivity extends Activity implements ControllerListener
{
	final ExamplePlayer player = new ExamplePlayer();
	GLSurfaceView mView = null;
	
	Controller controllerManager = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		controllerManager = Controller.getInstance(this);
		controllerManager.init();
		controllerManager.setListener(this, null);
		
		mView = new GLSurfaceView(this);
		mView.setRenderer(new ExampleRenderer());
		mView.setKeepScreenOn(true);
		((FrameLayout)findViewById(R.id.myframelayout)).addView(mView, 0);
	}
	
	@Override
	protected void onDestroy() 
	{
		controllerManager.exit();
		super.onDestroy();
	}
	
	protected void OnPause()
	{
		controllerManager.onPause();
		mView.onPause();
		super.onPause();
	}
	
	protected void OnResume()
	{
		super.onResume();
		mView.onResume();
		controllerManager.onResume();

		player.mConnection 		= controllerManager.getState(Controller.STATE_CONNECTION);
		player.mControllerModel = controllerManager.getState(Controller.STATE_CURRENT_PRODUCT_VERSION); 
		player.mButtonA 		= controllerManager.getKeyCode(Controller.KEYCODE_BUTTON_A);
		player.mButtonB 		= controllerManager.getKeyCode(Controller.KEYCODE_BUTTON_B);
		player.horizontal 		= controllerManager.getAxisValue(Controller.AXIS_X);
		player.vertical 		= controllerManager.getAxisValue(Controller.AXIS_Y);
		player.lookHorizontal 	= controllerManager.getAxisValue(Controller.AXIS_Z);
		player.lookVertical 	= controllerManager.getAxisValue(Controller.AXIS_RZ);
	}
	
	// Player ---------------------------------------------------------------------
	
	class ExamplePlayer
	{        
		int 			mConnection 		= StateEvent.ACTION_DISCONNECTED,
		 				mControllerModel 	= StateEvent.STATE_UNKNOWN,
		 				mButtonA 			= KeyEvent.ACTION_UP,
		 				mButtonB 			= KeyEvent.ACTION_UP;
		
        private float 	horizontal,
        				vertical,
        				lookHorizontal,
        				lookVertical,
        				xPos,
        				yPos;

		public ExamplePlayer()
		{	                  

		}
	}
    
    // Renderer ---------------------------------------------------------------------
    
	class ExampleRenderer implements GLSurfaceView.Renderer
	{
		final FloatBuffer mVertexBuffer;

		public ExampleRenderer()
		{
			final float[] vertices = {5.0f, 0.0f, 0.0f, 0.0f, 5.0f, 0.0f, -5.0f, 0.0f, 0.0f, 0.0f, -5.0f, 0.0f};
			final ByteBuffer byteBuffer = ByteBuffer.allocateDirect(vertices.length * 4);
			byteBuffer.order(ByteOrder.nativeOrder());
			mVertexBuffer = byteBuffer.asFloatBuffer();
			mVertexBuffer.put(vertices);
			mVertexBuffer.position(0);
		}

		@Override
		public void onDrawFrame(GL10 gl) {

			gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
			gl.glMatrixMode(GL10.GL_MODELVIEW);

			final float scale = 10.0f;
			player.xPos += (player.horizontal + player.lookHorizontal) * scale;
			player.yPos -= (player.vertical + player.lookVertical) * scale;

			gl.glLoadIdentity();
			gl.glTranslatef(player.xPos, player.yPos, 0.0f);
			gl.glScalef(10.0f, 10.0f, 10.0f);
			gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
			gl.glColor4f(1f, 0f, 0f, 1f);
			gl.glVertexPointer(3, GL10.GL_FLOAT, 0, mVertexBuffer);
				
			if (player.mConnection == Controller.ACTION_CONNECTED)
			{
				if (player.mControllerModel == Controller.ACTION_VERSION_MOGA) 
				{
					gl.glDrawArrays(GL10.GL_TRIANGLE_FAN, 0, 4);
				}
			}
			
			gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		}

		@Override
		public void onSurfaceChanged(GL10 gl, int width, int height) {

			gl.glViewport(0, 0, width, height);
			gl.glMatrixMode(GL10.GL_PROJECTION);
			gl.glLoadIdentity();
			gl.glOrthof(width / -2.0f, width / +2.0f, height / -2.0f, height / +2.0f, -1.0f, 1.0f);
			
		}

		@Override
		public void onSurfaceCreated(GL10 arg0, EGLConfig arg1) {
			// TODO Auto-generated method stub
			
		}
	}

	// Controller ----------------------------------------------------------------
	
	@Override
	public void onKeyEvent(KeyEvent event) 
	{
		switch (controllerManager.getState(Controller.STATE_CURRENT_PRODUCT_VERSION)) 
		{
			case Controller.ACTION_VERSION_MOGA:
				
			switch (event.getKeyCode()) 
			{
				case KeyEvent.KEYCODE_BUTTON_A:
				player.mButtonA = event.getAction();
				break;

				case KeyEvent.KEYCODE_BUTTON_B:
				player.mButtonB = event.getAction();
				break;
			}
			break;
				
			case Controller.ACTION_VERSION_MOGAPRO:
				
			switch (event.getKeyCode()) 
			{
				case KeyEvent.KEYCODE_BUTTON_X:
				player.mButtonA = event.getAction();
				break;

				case KeyEvent.KEYCODE_BUTTON_Y:
				player.mButtonB = event.getAction();
				break;
			}
				
			break;
		}
	}

	@Override
	public void onMotionEvent(MotionEvent event) 
	{
		player.horizontal 		= event.getAxisValue(MotionEvent.AXIS_X);
		player.vertical 		= event.getAxisValue(MotionEvent.AXIS_Y);
		player.lookHorizontal 	= event.getAxisValue(MotionEvent.AXIS_Z);
		player.lookVertical 	= event.getAxisValue(MotionEvent.AXIS_RZ);	
	}

	@Override
	public void onStateEvent(StateEvent event) 
	{
		switch (event.getState()) 
		{
		case StateEvent.STATE_CONNECTION:
			player.mConnection = event.getAction();
			break;
		case StateEvent.STATE_CURRENT_PRODUCT_VERSION:
			player.mControllerModel = event.getAction();
			break;
		}
	}
}
